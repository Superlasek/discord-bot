import os
import discord
from discord.ext import commands
from discord import Activity, ActivityType
from dotenv import load_dotenv
import urllib.request
import json
import random
import asyncio
import math
import datetime

# -------- CARGADO DEL BOT

load_dotenv() # Carga el archivo env
TOKEN = os.getenv('DISCORD_TOKEN')

client = discord.Client()

bot = commands.Bot(command_prefix='.uc ') #Prefijo del bot

@bot.event
async def on_ready():
    print('Cliente listo :)')


# -------- COMANDOS

async def background_loop():
    await client.wait_until_ready()
    while not client.is_closed:
        channel = client.get_channel("497205929759211541")
        messages = ["Hola!", "¿Como te encuentras Oni-chan?~", "uwu"]
        await client.send_message(channel, random.choice(messages))
        await asyncio.sleep(10)


@bot.command(name='op')
async def op(ctx, num1, signo, num2):
    if signo == '+':
        response = int(num1) + int(num2)
        await ctx.send(response)
    elif signo == '-':
        response = int(num1) - int(num2)
        await ctx.send(response)
    elif signo == '*':
        response = int(num1) * int(num2)
        await ctx.send(response)
    elif signo == '/':
        try:
            response = int(num1) / int(num2)
            await ctx.send(response)
        except:
            await ctx.send('No se puede dividir entre 0 \nBaka... (￣_￣|||)')
    elif signo == '^':
        response = pow(int(num1), int(num2))
        await ctx.send(response)
    elif signo == '√':
        response = math.sqrt(int(num1))
        await ctx.send(response)



#@bot.command(name='ayuda')
#async def ayuda(ctx):

    #embed = discord.Embed(
    #    colour = discord.Colour.green()
    #)
    #embed.set_author(name='Ayuda')
    #embed.add_field(name='.uc ping', value='Pong uwu', incline=False)

@bot.command(name='ping')
async def ping(ctx):
    await ctx.send('Pong （￣︶￣)🏓')
    messages = ["¡Te gane! (≧∀≦)", "Perdí... (´。＿。｀)"]
    await ctx.send(random.choice(messages))

@bot.command(name='sabia') #Parte Favorita de Paul
async def sabia(ctx, messages):
    await ctx.send('🔮 La Loli Sabia dice:')
    messages = ["Tal vez", "Definitivamente Si", "Definitivamente No", "Puede Ser"]
    await ctx.send(random.choice(messages))

@bot.command()
async def hola(ctx):
    await ctx.send('Buenas Oni-chan (✿◡‿◡)')

@bot.command()
async def Hola(ctx):
    await ctx.send('Buenas Oni-chan (✿◡‿◡)')


@bot.command()
async def info(ctx):
    embed = discord.Embed(title=f"{ctx.guild.name}", description="Información breve del servidor. ", timestamp=datetime.datetime.utcnow(), color=discord.Color.green())
    embed.add_field(name="Servidor creado el", value=f"{ctx.guild.created_at}")
    embed.add_field(name="Dueño servidor", value=f"{ctx.guild.owner}")
    embed.add_field(name="Server Region", value=f"{ctx.guild.region}")
    embed.add_field(name="Server ID", value=f"{ctx.guild.id}")
    # embed.set_thumbnail(url=f"{ctx.guild.icon}")
    embed.set_thumbnail(url="https://pluralsight.imgix.net/paths/python-7be70baaac.png")

    await ctx.send(embed=embed)



#@bot.event
#async def on_message(mensaje):
#    if mensaje.content.find('xd')!=-1:
#        await mensaje.channel.send('XD')
#    if mensaje.content.find(':v')!=-1:
#        await mensaje.channel.send('Baia baia, un grasoso')
#    if mensaje.content.find('puto')!=-1:
#        await mensaje.channel.send('hey tum tum que grosero')

@client.event
async def on_message(message):
    # Simple return para que no se responda a si mismo
    if message.author == client.user:
        return

    if message.content.startswith('puto'):
        msg = 'Hey {0.author.mention} que grosero'.format(message)
        await client.send_message(message.channel, msg)
    if message.content.startswith('hey'):
        msg = 'tum tum que grosero'.format(message)
        await client.send_message(message.channel, msg)

bot.loop.create_task(background_loop())

bot.run(TOKEN) # Run del bot